from seleniumwire import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
import time
import csv


def browser_init():
    # Инициализация работы браузера
    # Для работы использована прокси https://proxy2.webshare.io/ 
    proxy_username = 'yplrhyho'
    proxy_password = '1yqnldmrgy15'
    seleniumwire_options = {
        'proxy': {
            'http': f'http://{proxy_username}:{proxy_password}@185.199.229.156:7492',
            'verify_ssl': True,
        },
    }
    # Дополнительно подменяем заголовки
    useragent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
    service=ChromeService(ChromeDriverManager().install())
    options = webdriver.ChromeOptions()
    # Отключение функций, при которых Chrome определяет что он управляется с помощью скриптов автоматизации
    options.add_argument("--disable-blink-features=AutomationControlled") 
    options.add_experimental_option("excludeSwitches", ["enable-automation"]) 
    options.add_experimental_option("useAutomationExtension", False) 
    driver = webdriver.Chrome(service=service, seleniumwire_options=seleniumwire_options, options=options)
    driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")
    driver.execute_cdp_cmd("Network.setUserAgentOverride", {"userAgent": useragent})
    return driver

def main():
    # Получение страницы с 'Pre-Open Market' и сбор данных всех имен и финальных цен
    browser = browser_init()
    browser.get("https://www.nseindia.com ")
    data = []
    # Использование time является костылем, в дальнейшем будет заменен на функцию selenium WebDriverWait
    time.sleep(10)
    assert "NSE" in browser.title
    menu = browser.find_element(By.LINK_TEXT, 'MARKET DATA')
    actions = ActionChains(browser)
    actions.move_to_element(menu).click(menu).perform()
    hidden_menu = browser.find_element(By.LINK_TEXT, 'Pre-Open Market')
    actions.move_to_element(hidden_menu).click(hidden_menu).perform()
    time.sleep(10)
    # скрейпинг с помощью BeatifulSoup по двум колонкам Name, Final
    soup = BeautifulSoup(browser.page_source, 'lxml')
    all_names = soup.find_all('a', {'class':'symbol-word-break'})
    all_final_price = soup.find_all('td', {'class':'bold text-right'})
    for name, price in zip(all_names, all_final_price):
        data.append({'NAME': name.text, 'PRICE': price.text})
    return [data,browser]

def save(data):
    #Запись данных из main в csv файл
    with open('data.csv', 'w', encoding='UTF8') as f:
        writer = csv.DictWriter(f, fieldnames=['NAME', 'PRICE'], delimiter=';')
        writer.writeheader()
        writer.writerows(data)

def browsing(browser):
    #Имитация деятельности пользователя

    actions = ActionChains(browser)
    
    menu = browser.find_element(By.LINK_TEXT, 'HOME')
    actions.move_to_element(menu).click(menu).perform()
    time.sleep(3)

    bank = browser.find_element(By.ID, 'tabList_NIFTYBANK')
    actions.move_to_element(bank).click(bank).perform()
    time.sleep(1)

    view_all = browser.find_element(By.XPATH,"//*[@id='tab4_gainers_loosers']/div[3]/a")
    browser.execute_script('arguments[0].scrollIntoView({block: "center", behavior: "smooth" });', view_all)
    time.sleep(2)
    actions.move_to_element(view_all).click(view_all).perform()
    time.sleep(3)

    drop_menu = browser.find_element(By.ID, 'equitieStockSelect')
    actions.move_to_element(drop_menu).click(drop_menu).perform()
    time.sleep(3)
    needed_elem = browser.find_element(By.XPATH,"//*[@id='equitieStockSelect']/optgroup[4]/option[7]")
    #По какйо-то причине не хочет прокручивать выпадающий список. Пока проблема
    browser.execute_script('arguments[0].scrollIntoView(true);', needed_elem)
    time.sleep(7)
    actions.move_to_element(drop_menu).click(drop_menu).perform()
    needed_elem.click()
    time.sleep(5)
    end_table = browser.find_element(By.ID, 'marketWatchEquityCmsNote')
    browser.execute_script('arguments[0].scrollIntoView({ block: "nearest", behavior: "smooth" });', end_table)
    time.sleep(6)
    browser.close()



if __name__ == "__main__":
    data = main()
    save(data[0])
    time.sleep(3)
    browsing(data[1])