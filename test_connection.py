from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
import time

# define custom options for the Selenium driver
options = Options()
# free proxy server URL
proxy_server_url = "134.209.105.209:3128"
options.add_argument(f'--proxy-server={proxy_server_url}')

# create the ChromeDriver instance with custom options
driver = webdriver.Chrome(
    service=ChromeService(ChromeDriverManager().install()),
    options=options
)
driver.get('http://httpbin.org/ip')
time.sleep(300)