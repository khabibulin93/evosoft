from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from bs4 import BeautifulSoup
import time
# Экспортируем функцию инициализации окна браузера
from parsing_nseindia import browser_init


def main():
    """
    Последовательность действий

        1.Открыть гугл
        2.Запросить твитер илона маска
        3.Выбрать ссылку с илоном
        4.Спарсить твиты
    
    """
    browser = browser_init()
    actions = ActionChains(browser)
    browser.get("https://www.google.ru/")
    time.sleep(5)
    assert "Google" in browser.title
    elem = browser.find_element(By.NAME, "q")
    elem.send_keys("twitter elon musk")
    time.sleep(2)
    elem.send_keys(Keys.RETURN)
    time.sleep(5)
    """ 
    Здесь введен блок try, так как гугл выдает два разных состояния блоков, на первую ссылку, 
    с заголовками с твиттера, и без. В конце в случае если появится каптча, он открывает напрямую страницу твиттера

    """
    try:
        href_twitter_header = browser.find_element(By.XPATH,'//*[@id="rso"]/div[1]/div/div[2]/div/g-section-with-header/div[1]/div/div[1]/g-link/a')
        actions.move_to_element(href_twitter_header).click(href_twitter_header).perform()
    except NoSuchElementException:
        href_twitter = browser.find_element(By.XPATH,'//*[@id="rso"]/div[1]/div/div/div/div/div/div/div[1]/div/span/a')
        actions.move_to_element(href_twitter).click(href_twitter).perform()
    except Exception:
        browser.get("https://twitter.com/elonmusk")
    # Тут пока тоже костыли с ожиданием
    time.sleep(30)
    # Переключаем фокус драйвера на последнюю открытую вкладку
    browser.switch_to.window(browser.window_handles[-1])
    # Имитируем прокрутку страницы
    i=0
    while i < 3:
        actions.key_down('\ue00f').key_up('\ue00f').perform()
        time.sleep(2)
        i+=1
    time.sleep(5)
    # Скрейпинг текста твитов
    soup = BeautifulSoup(browser.page_source, "lxml")
    find_text = soup.find_all(attrs = {"data-testid": "tweetText"}, limit=10)
    time.sleep(1)
    for res in find_text:
        text_span = res.find_all('span')
        for texts in text_span:
            # Выводим в лог
            print (texts.text)

if __name__ == "__main__":
    main()